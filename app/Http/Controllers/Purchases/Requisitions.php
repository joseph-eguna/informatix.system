<?php

namespace App\Http\Controllers\Purchases;

use App\Http\Controllers\Controller;
use App\Http\Requests\Purchase\Requisition as Request;
use App\Models\Purchase\Requisition;
use App\Models\Purchase\RequisitionItem;

/*use App\Models\Expense\Vendor;
use App\Models\Item\Item;
use App\Models\Setting\Currency;
use App\Models\Setting\Tax;*/

use Auth;

class Requisitions extends Controller
{
    //use DateTime, Currencies, Uploads;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Requisition $requisition)
    {
        
        //must be coming from database change it soon
        $status = [
            ''=>'All',
            'for-approval'=>'For Approval',
            'approved'=>'Approved',
            'declined'=>'Declined',
        ];

        $status_codes = [
            'for-approval'=>'bg-aqua',
            'approved'=>'label-success',
            'declined'=>'label-danger',
        ];

        $user = Auth::user();
        $user_companies = [];
        foreach($user->companies as $company){
            array_push($user_companies, $company->id);
        }
        
        $requisitions = $requisition->whereIn('company_id', $user_companies)->get();

        return view('purchases.requisitions.index', compact('requisitions', 'status', 'status_codes'));
    }


    /**
     * Show the form for viewing the specified resource.
     *
     * @param  Bill  $bill
     *
     * @return Response
     */
    public function show(Requisition $requisition)
    {
      
        $status_codes = [
            'for-approval'=>'bg-aqua',
            'approved'=>'label-success',
            'declined'=>'label-danger',
        ];

        return view('purchases.requisitions.show', compact('requisition', 'status_codes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('purchases.requisitions.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  
     *
     * @return 
     */
    public function edit(Requisition $requisition)
    {

        if(in_array($requisition->request_status_code, ['declined','approved'])) {
            return redirect('purchases/requisitions/'.$requisition->id);
        }

        return view('purchases.requisitions.edit', compact('requisition'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $request['company_id']  = session('company_id');
        $request['user_id']     = Auth::user()->id;
        $request['remarks']     = $request['notes'];
        $request['amount']      = 0;
        $request['request_status_code'] = 'for-approval';

        $requisition = Requisition::create($request->input());

        //['requisition_id', 'name', 'sku', 'quantity', 'price', 'total']

        $sum = 0;

        if(!empty($request['item'])) {
            
            foreach($request['item'] as $item){
                
                $total = floatval($item['quantity'] * $item['price']);

                $items = [
                    'requisition_id'    => $requisition->id, 
                    'name'              => $item['name'], 
                    'quantity'          => $item['quantity'], 
                    'price'             => $item['price'], 
                    'total'             => $total
                ];

                RequisitionItem::create($items);
                
                $sum = $sum + $total;
            }
        }
        
        $requisition->amount = $sum;

        $requisition->save();

        //return $request->input();

        return redirect('purchases/requisitions/');

    }


    /**
     * Duplicate the specified resource.
     *
     * @param  Bill  $bill
     *
     * @return Response
     */
    public function duplicate(Bill $bill)
    {
        $clone = $bill->duplicate();

        // Add bill history
        BillHistory::create([
            'company_id' => session('company_id'),
            'bill_id' => $clone->id,
            'status_code' => 'draft',
            'notify' => 0,
            'description' => trans('messages.success.added', ['type' => $clone->bill_number]),
        ]);

        $message = trans('messages.success.duplicated', ['type' => trans_choice('general.bills', 1)]);

        flash($message)->success();

        return redirect('expenses/bills/' . $clone->id . '/edit');
    }

    /**
     * Import the specified resource.
     *
     * @param  ImportFile  $import
     *
     * @return Response
     */
    public function import(ImportFile $import)
    {
        $rows = $import->all();

        foreach ($rows as $row) {
            $data = $row->toArray();
            $data['company_id'] = session('company_id');

            Bill::create($data);
        }

        $message = trans('messages.success.imported', ['type' => trans_choice('general.bills', 2)]);

        flash($message)->success();

        return redirect('expenses/bills');
    }

   
    /**
     * Update the specified resource in storage.
     *
     * @param  
     * @param  
     *
     * @return Response
     */
    public function update(Requisition $requisition, Request $request)
    {

        $request['remarks'] = $request['notes'];

        $requisition->update($request->input());

        RequisitionItem::where('requisition_id', $requisition->id)->delete();
        
        
        $sum = 0;

        if(!empty($request['item'])) {
            
            foreach($request['item'] as $item){
                
                $total = floatval($item['quantity'] * $item['price']);

                $items = [
                    'requisition_id'    => $requisition->id, 
                    'name'              => $item['name'], 
                    'quantity'          => $item['quantity'], 
                    'price'             => $item['price'], 
                    'total'             => $total
                ];

                RequisitionItem::create($items);
                
                $sum = $sum + $total;
            }
        }
        
        $requisition->amount = $sum;

        $requisition->save();


        //return $request->input();

        return redirect('purchases/requisitions/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Requisition $requisition
     *
     * @return Response
     */
    public function destroy(Requisition $requisition)
    {
        
        /*
        BillItem::where('bill_id', $bill->id)->delete();
        BillTotal::where('bill_id', $bill->id)->delete();
        BillPayment::where('bill_id', $bill->id)->delete();
        BillHistory::where('bill_id', $bill->id)->delete();
        */

        if($requisition->request_status_code != 'for-approval') {
        
            $message = trans('Unable to delete', ['type' => trans_choice('general.bills', 1)]);
            
        } else {

            $requisition->delete();
            RequisitionItem::where('requisition_id', $requisition->id)->delete();
        
        }

        flash($message)->success();

        return redirect('purchases/requisitions');
    }

    /**
     * Decline as Request.
     *
     * @param  
     *
     * @return Response
     */
    public function decline(Requisition $requisition)
    {

        $requisition->checker_id = Auth::user()->id;

        $requisition->request_status_code = 'declined';
       
        $requisition->save();

        flash(trans('Request Declined'))->success();

        return redirect()->back();
    }

    /**
     * Approved Request.
     *
     * @param  
     *
     * @return Response
     */
    public function approve(Requisition $requisition)
    {
        $requisition->checker_id = Auth::user()->id;

        $requisition->request_status_code = 'approved';
       
        $requisition->save();

        flash(trans('Request Approved'))->success();

        return redirect()->back();
    }

    /**
     * Print the bill.
     *
     * @param  Bill $bill
     *
     * @return Response
     */
    public function printBill(Bill $bill)
    {
        $paid = 0;

        foreach ($bill->payments as $item) {
            $item->default_currency_code = $bill->currency_code;

            $paid += $item->getDynamicConvertedAmount();
        }

        $bill->paid = $paid;

        return view('expenses.bills.bill', compact('bill'));
    }

    /**
     * Download the PDF file of bill.
     *
     * @param  Bill $bill
     *
     * @return Response
     */
    public function pdfBill(Bill $bill)
    {
        $paid = 0;

        foreach ($bill->payments as $item) {
            $item->default_currency_code = $bill->currency_code;

            $paid += $item->getDynamicConvertedAmount();
        }

        $bill->paid = $paid;

        $html = view('expenses.bills.bill', compact('bill'))->render();

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($html);

        $file_name = 'bill_'.time().'.pdf';

        return $pdf->download($file_name);
    }

    /**
     * Add payment to the bill.
     *
     * @param  PaymentRequest  $request
     *
     * @return Response
     */
    public function payment(PaymentRequest $request)
    {
        // Get currency object
        $currency = Currency::where('code', $request['currency_code'])->first();

        $request['currency_code'] = $currency->code;
        $request['currency_rate'] = $currency->rate;

        $bill = Bill::find($request['bill_id']);

        $total_amount = $bill->amount;

        $amount = (double) $request['amount'];

        if ($request['currency_code'] != $bill->currency_code) {
            $request_bill = new Bill();

            $request_bill->amount = (float) $request['amount'];
            $request_bill->currency_code = $currency->code;
            $request_bill->currency_rate = $currency->rate;

            $amount = $request_bill->getConvertedAmount();
        }

        if ($bill->payments()->count()) {
            $total_amount -= $bill->payments()->paid();
        }

        if ($amount > $total_amount) {
            $message = trans('messages.error.payment_add');

            return response()->json([
                'success' => false,
                'error' => true,
                'message' => $message,
            ]);
        } elseif ($amount == $total_amount) {
            $bill->bill_status_code = 'paid';
        } else {
            $bill->bill_status_code = 'partial';
        }

        $bill->save();

        $bill_payment = BillPayment::create($request->input());

        // Upload attachment
        if ($request->file('attachment')) {
            $media = $this->getMedia($request->file('attachment'), 'bills');

            $bill_payment->attachMedia($media, 'attachment');
        }

        $request['status_code'] = $bill->bill_status_code;
        $request['notify'] = 0;

        $desc_amount = money((float) $request['amount'], (string) $request['currency_code'], true)->format();

        $request['description'] = $desc_amount . ' ' . trans_choice('general.payments', 1);

        BillHistory::create($request->input());

        $message = trans('messages.success.added', ['type' => trans_choice('general.revenues', 1)]);

        return response()->json([
            'success' => true,
            'error' => false,
            'message' => $message,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  BillPayment  $payment
     *
     * @return Response
     */
    public function paymentDestroy(BillPayment $payment)
    {
        $bill = Bill::find($payment->bill_id);

        if ($bill->payments()->count() > 1) {
            $bill->bill_status_code = 'partial';
        } else {
            $bill->bill_status_code = 'draft';
        }

        $bill->save();

        $payment->delete();

        $message = trans('messages.success.deleted', ['type' => trans_choice('general.bills', 1)]);

        flash($message)->success();

        return redirect()->back();
    }
}
