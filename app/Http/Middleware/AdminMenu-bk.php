<?php

namespace App\Http\Middleware;

use App\Models\Module\Module;
use App\Events\AdminMenuCreated;
use Auth;
use Closure;
use Menu;
use Module as LaravelModule;

class AdminMenu
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Check if logged in
        if (!Auth::check()) {
            return $next($request);
        }

        // Setup the admin menu
        Menu::create('AdminMenu', function ($menu) {
        
            $menu->style('adminlte');

            $user = Auth::user();
            $attr = ['icon' => 'fa fa-angle-double-right'];

            // Dashboard
            $menu->add([
                'url' => '/',
                'title' => trans('general.dashboard'),
                'icon' => 'fa fa-dashboard',
                'order' => 1,
            ]);


            //Contacts
            if ($user->can(['read-incomes-invoices', 'read-incomes-revenues', 'read-incomes-customers'])) {
                $menu->dropdown(trans_choice('Contacts', 2), function ($sub) use($user, $attr) {
                    
                    if ($user->can('read-incomes-invoices')) {
                        $sub->url('contacts/clients', trans_choice('Clients', 2), 1, $attr);
                    }

                    if ($user->can('read-incomes-invoices')) {
                        $sub->url('expenses/vendors', trans_choice('Suppliers', 2), 2, $attr);
                    }

                    if ($user->can('read-incomes-invoices')) {
                        $sub->url('contacts/employees', trans_choice('Employees', 2), 2, $attr);
                    }

                }, 2, [
                    'title' => trans_choice('Contacts', 2),
                    'icon' => 'fa fa-address-book',
                ]);
            }

            
            //Sales
            if ($user->can(['read-incomes-invoices', 'read-incomes-revenues', 'read-incomes-customers'])) {
                $menu->dropdown(trans_choice('general.sales', 2), function ($sub) use($user, $attr) {
                    

                    if ($user->can('read-incomes-revenues')) {
                        $sub->url('incomes/revenues', trans_choice('general.revenues', 2), 1, $attr);
                    }

                    if ($user->can('read-incomes-invoices')) {
                        $sub->url('incomes/invoices', trans_choice('general.invoices', 2), 2, $attr);
                    }

                    if ($user->can('read-incomes-invoices')) {
                        $sub->url('incomes/invoices', trans_choice('Invoice Statement', 2), 3, $attr);
                    }

                    /*if ($user->can('read-incomes-revenues')) {
                        $sub->url('incomes/revenues', trans_choice('general.revenues', 2), 2, $attr);
                    }*/

                    /*if ($user->can('read-incomes-customers')) {
                        $sub->url('incomes/customers', trans_choice('general.customers', 2), 3, $attr);
                    }*/
                }, 5, [
                    'title' => trans_choice('general.incomes', 2),
                    'icon' => 'fa fa-money',
                ]);
            }

            //Purchase Requisition
            /*if ($user->can(['read-incomes-invoices', 'read-incomes-revenues', 'read-incomes-customers'])) {
                $menu->dropdown(trans_choice('Purchase Requisitions', 2), function ($sub) use($user, $attr) {
                    
                    if ($user->can('read-incomes-invoices')) {
                        $sub->url('purchases/requisitions', trans_choice('Lists', 2), 1, $attr);
                    }

                    if ($user->can('read-incomes-invoices')) {
                        $sub->url('purchases/requests', trans_choice('Purchase Requests', 2), 2, $attr);
                    }

                }, 6, [
                    'title' => trans_choice('general.incomes', 2),
                    'icon' => 'fa fa-download',
                ]);
            }*/

            if ($user->can(['read-expenses-bills', 'read-expenses-payments', 'read-expenses-vendors'])) {
                $menu->dropdown(trans_choice('Purchase Requisitions', 2), function ($sub) use($user, $attr) {
                    
                    /*if ($user->can('read-expenses-bills')) {
                        $sub->url('expenses/bills', trans_choice('Purchases', 2), 1, $attr);
                    }
                    if ($user->can('read-expenses-bills')) {
                        $sub->url('expenses/bills/create', trans_choice('Create Purchase', 2), 2, $attr);
                    }
                   
                    /*if ($user->can('read-expenses-payments')) {
                        $sub->url('expenses/payments', trans_choice('general.payments', 2), 2, $attr);
                    }*/

                    if ($user->can('read-purchases-requisitions')) {
                        $sub->url('purchases/requisitions', trans_choice('Requisitions', 2), 3, $attr);
                    }

                }, 6, [
                    'title' => trans_choice('general.expenses', 2),
                    'icon' => 'fa fa-download',
                ]);
            }

            //Petty Cash Funds
            if ($user->can(['read-incomes-invoices', 'read-incomes-revenues', 'read-incomes-customers'])) {
                $menu->dropdown(trans_choice('Petty Cash Funds', 2), function ($sub) use($user, $attr) {
                    
                    if ($user->can('read-incomes-invoices')) {
                        $sub->url('pcf/lists', trans_choice('Lists', 2), 1, $attr);
                    }

                    if ($user->can('read-incomes-invoices')) {
                        $sub->url('pcf/requests', trans_choice('PCF Requests', 2), 2, $attr);
                    }

                }, 7, [
                    'title' => trans_choice('general.incomes', 2),
                    'icon' => 'fa fa-envelope',
                ]);
            }

            //Project Management
            if ($user->can(['read-incomes-invoices', 'read-incomes-revenues', 'read-incomes-customers'])) {
                $menu->dropdown(trans_choice('Project Management', 2), function ($sub) use($user, $attr) {
                    
                    if ($user->can('read-incomes-invoices')) {
                        $sub->url('project-management/usage', trans_choice('Material Usage', 2), 1, $attr);
                    }

                    if ($user->can('read-incomes-invoices')) {
                        $sub->url('project-management/labor', trans_choice('Direct Labor', 2), 2, $attr);
                    }

                }, 8, [
                    'title' => trans_choice('general.incomes', 2),
                    'icon' => 'fa fa-home',
                ]);
            }
           
            // Items
            /*if ($user->can('read-expenses-vendors')) {
                $menu->add([
                    'url' => 'items/items',
                    'title' => trans_choice('Inventory', 10),
                    'icon' => 'fa fa-cubes',
                    'order' =>  10,
                ]);
            }

             // Items
            if ($user->can('read-expenses-vendors')) {
                $menu->add([
                    'url' => 'items/items',
                    'title' => trans_choice('Disbursement', 10),
                    'icon' => 'fa fa-adjust',
                    'order' => 11,
                ]);
            }*/


            // Incomes
            /*if ($user->can(['read-incomes-invoices', 'read-incomes-revenues', 'read-incomes-customers'])) {
                $menu->dropdown(trans_choice('general.incomes', 2), function ($sub) use($user, $attr) {
                    if ($user->can('read-incomes-invoices')) {
                        $sub->url('incomes/invoices', trans_choice('general.invoices', 2), 1, $attr);
                    }

                    if ($user->can('read-incomes-revenues')) {
                        $sub->url('incomes/revenues', trans_choice('general.revenues', 2), 2, $attr);
                    }

                    if ($user->can('read-incomes-customers')) {
                        $sub->url('incomes/customers', trans_choice('general.customers', 2), 3, $attr);
                    }
                }, 13, [
                    'title' => trans_choice('general.incomes', 2),
                    'icon' => 'fa fa-money',
                ]);
            }*/

            // Expences
            /*if ($user->can(['read-expenses-bills', 'read-expenses-payments', 'read-expenses-vendors'])) {
                $menu->dropdown(trans_choice('general.expenses', 2), function ($sub) use($user, $attr) {
                    if ($user->can('read-expenses-bills')) {
                        $sub->url('expenses/bills', trans_choice('general.bills', 2), 1, $attr);
                    }

                    if ($user->can('read-expenses-payments')) {
                        $sub->url('expenses/payments', trans_choice('general.payments', 2), 2, $attr);
                    }

                }, 14, [
                    'title' => trans_choice('general.expenses', 2),
                    'icon' => 'fa fa-shopping-cart',
                ]);
            }*/

            // Banking
            /*if ($user->can(['read-banking-accounts', 'read-banking-transfers', 'read-banking-transactions'])) {
                $menu->dropdown(trans('general.banking'), function ($sub) use($user, $attr) {
                    if ($user->can('read-banking-accounts')) {
                        $sub->url('banking/accounts', trans_choice('general.accounts', 2), 1, $attr);
                    }

                    if ($user->can('read-banking-transfers')) {
                        $sub->url('banking/transfers', trans_choice('general.transfers', 2), 2, $attr);
                    }

                    if ($user->can('read-banking-transactions')) {
                        $sub->url('banking/transactions', trans_choice('general.transactions', 2), 3, $attr);
                    }
                }, 15, [
                    'title' => trans('general.banking'),
                    'icon' => 'fa fa-university',
                ]);
            }*/

            // Reports
            if ($user->can(['read-reports-income-summary', 'read-reports-expense-summary', 'read-reports-income-expense-summary'])) {
                $menu->dropdown(trans_choice('general.reports', 2), function ($sub) use($user, $attr) {
                    if ($user->can('read-reports-income-summary')) {
                        $sub->url('reports/income-summary', trans('reports.summary.income'), 1, $attr);
                    }

                    if ($user->can('read-reports-expense-summary')) {
                        $sub->url('reports/expense-summary', trans('reports.summary.expense'), 2, $attr);
                    }

                    if ($user->can('read-reports-income-expense-summary')) {
                        $sub->url('reports/income-expense-summary', trans('reports.summary.income_expense'), 3, $attr);
                    }
                }, 16, [
                    'title' => trans_choice('general.reports', 2),
                    'icon' => 'fa fa-bar-chart',
                ]);
            }

            // Settings
            if ($user->can(['read-settings-settings', 'read-settings-categories', 'read-settings-currencies', 'read-settings-taxes'])) {
                $menu->dropdown(trans_choice('general.settings', 2), function ($sub) use($user, $attr) {
                    if ($user->can('read-settings-settings')) {
                        $sub->url('settings/settings', trans('general.general'), 1, $attr);
                    }

                    if ($user->can('read-settings-categories')) {
                        $sub->url('settings/categories', trans_choice('general.categories', 2), 2, $attr);
                    }

                    if ($user->can('read-settings-currencies')) {
                        $sub->url('settings/currencies', trans_choice('general.currencies', 2), 3, $attr);
                    }

                    if ($user->can('read-settings-taxes')) {
                        $sub->url('settings/taxes', trans_choice('general.tax_rates', 2), 4, $attr);
                    }

                    // Modules
                    $modules = Module::all();
                    $position = 5;
                    foreach ($modules as $module) {
                        $m = LaravelModule::findByAlias($module->alias);

                        // Check if the module has settings
                        if (empty($m->get('settings'))) {
                            continue;
                        }

                        $sub->url('settings/apps/' . $m->getAlias(), title_case(str_replace('_', ' ', snake_case($m->getName()))), $position, $attr);

                        $position++;
                    }
                }, 17, [
                    'title' => trans_choice('general.settings', 2),
                    'icon' => 'fa fa-gears',
                ]);
            }

            // Apps
            if ($user->can('read-modules-home')) {
                $menu->add([
                    'url' => 'apps/home',
                    'title' => trans_choice('general.modules', 2),
                    'icon' => 'fa fa-rocket',
                    'order' => 18,
                ]);
            }

            // Fire the event to extend the menu
            event(new AdminMenuCreated($menu));
        });

        return $next($request);
    }
}