<?php

namespace App\Models\Purchase;

use App\Models\Model;

//use Illuminate\Database\Eloquent\Model;
/*
use App\Traits\Currencies;
use App\Traits\DateTime;
use Bkwld\Cloner\Cloneable;
use Sofa\Eloquence\Eloquence;
use App\Traits\Media;
*/

class Requisition extends Model
{
    //use Cloneable, Currencies, DateTime, Eloquence, Media;

    protected $table = 'requisitions';

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['company_id', 'user_id', 'checker_id', 'priority', 'remarks', 'amount', 'request_status_code', 'created_at', 'updated_at'];

    /**
     * Sortable columns.
     *
     * @var array
     */
    public $sortable = ['id','priority', 'remarks', 'request_status_code', 'created_at', 'updated_at'];

    /**
     * Searchable rules.
     *
     * @var array
     *
    protected $searchableColumns = [
        'purchase_number'    => 10,
        'order_number'   => 10,
        'vendor_name'    => 10,
        'vendor_email'   => 5,
        'vendor_phone'   => 2,
        'vendor_address' => 1,
        'notes'          => 2,
    ];

    protected $cloneable_relations = ['items', 'totals'];
    */
    
    public function company()
    {
        return $this->belongsTo('App\Models\Company\Company');
    }

    public function items()
    {
        return $this->hasMany('App\Models\Purchase\RequisitionItem', 'requisition_id');
    }

    public function account()
    {
        return $this->hasMany('App\Models\Setting\Setting', 'company_id', 'company_id');
    }
    
    public function createdby()
    {
        return $this->belongsTo('App\Models\Auth\User', 'user_id', 'id');
    }

    public function checkedby()
    {
        return $this->belongsTo('App\Models\Auth\User', 'checker_id', 'id');
    }
}
