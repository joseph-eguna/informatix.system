<?php

namespace App\Models\Purchase;

use Illuminate\Database\Eloquent\Model;

class RequisitionItem extends Model
{

    //use Currencies;

    protected $table = 'requisitions_items';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['requisition_id', 'name', 'sku', 'quantity', 'price', 'total'];

    /*public function bill()
    {
        return $this->belongsTo('App\Models\Expense\Bill');
    }

    public function item()
    {
        return $this->belongsTo('App\Models\Item\Item');
    }

    public function tax()
    {
        return $this->belongsTo('App\Models\Setting\Tax');
    }

   
    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = (double) $value;
    }

   
    public function setTotalAttribute($value)
    {
        $this->attributes['total'] = (double) $value;
    }*/
}   
