<?php

return [

    'domain'                => 'Domain',
    'logo'                  => 'Logo',
    'manage'                => 'Manage Projects',
    'all'                   => 'All Projects',
    'error' => [
        'delete_active'     => 'Error: Can not delete active project, please, change it first!',
    ],

];
