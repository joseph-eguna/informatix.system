@extends('layouts.admin')

@section('title', trans('general.dashboard'))

@section('content')

    <div class="row">
        <div class="col-md-4">
            <div class="info-box">
                <span class="info-box-icon label-warning"><i class="fa fa-folder-open"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Purchase Requisitions</span>
                    <span class="info-box-number">₱5520.00</span>
                    <div class="progress-group" title="" data-toggle="tooltip" data-html="true" data-original-title="">
                        <div class="progress sm">
                            <div class="progress-bar progress-bar-aqua" style="width: 100%"></div>
                        </div>
                        <span class="progress-text">Unapproved</span>
                        <span class="progress-number">₱5520.00 / ₱1,000.00</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="info-box">
                <span class="info-box-icon label-danger"><i class="fa fa-shopping-basket"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Purchase Orders</span>
                    <span class="info-box-number">₱2500.00</span>
                    <div class="progress-group" title="" data-toggle="tooltip" data-html="true" data-original-title="">
                        <div class="progress sm">
                            <div class="progress-bar progress-bar-aqua" style="width: 100%"></div>
                        </div>
                        <span class="progress-text">Payable</span>
                        <span class="progress-number">₱2500.00 / ₱1000.00</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="info-box">
                <span class="info-box-icon label-danger"><i class="fa fa-money"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Petty Cash Fund</span>
                    <span class="info-box-number">₱1500.00</span>
                    <div class="progress-group" title="" data-toggle="tooltip" data-html="true" data-original-title="">
                        <div class="progress sm">
                            <div class="progress-bar progress-bar-aqua" style="width: 100%"></div>
                        </div>
                        <span class="progress-text">Approved</span>
                        <span class="progress-number">₱1500.00 / ₱3,000.00</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-cubes"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Materials Usage</span>
                    <span class="info-box-number">85%</span>
                    <div class="progress-group" title="" data-toggle="tooltip" data-html="true" data-original-title="">
                        <div class="progress sm">
                            <div class="progress-bar progress-bar-aqua" style="width: 100%"></div>
                        </div>
                        <span class="progress-text">Percentage</span>
                        <span class="progress-number">85 / 100</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="info-box">
                <span class="info-box-icon label-success"><i class="fa fa-suitcase"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Contract Amount</span>
                    <span class="info-box-number">₱851,000.00</span>
                    <div class="progress-group" title="" data-toggle="tooltip" data-html="true" data-original-title="">
                        <div class="progress sm">
                            <div class="progress-bar progress-bar-aqua" style="width: 100%"></div>
                        </div>
                        <span class="progress-text">Paid</span>
                        <span class="progress-number">₱121,000.00 / ₱851,000.00</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="info-box">
                <span class="info-box-icon"><i class="fa fa-user-circle"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Clients</span>
                    <span class="info-box-number">--</span>
                    <div class="progress-group" title="" data-toggle="tooltip" data-html="true" data-original-title="">
                        <div class="progress sm">
                            <div class="progress-bar progress-bar-aqua" style="width: 100%"></div>
                        </div>
                        <span class="progress-text">--</span>
                        <span class="progress-number">--</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="info-box">
                <span class="info-box-icon"><i class="fa fa-user-circle"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Suppliers</span>
                    <span class="info-box-number">--</span>
                    <div class="progress-group" title="" data-toggle="tooltip" data-html="true" data-original-title="">
                        <div class="progress sm">
                            <div class="progress-bar progress-bar-aqua" style="width: 100%"></div>
                        </div>
                        <span class="progress-text">--</span>
                        <span class="progress-number">--</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="info-box">
                <span class="info-box-icon"><i class="fa fa-user-circle"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Employees</span>
                    <span class="info-box-number">--</span>
                    <div class="progress-group" title="" data-toggle="tooltip" data-html="true" data-original-title="">
                        <div class="progress sm">
                            <div class="progress-bar progress-bar-aqua" style="width: 100%"></div>
                        </div>
                        <span class="progress-text">--</span>
                        <span class="progress-number">--</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('css')

@endpush

@push('js')

@endpush

@push('scripts')

@endpush