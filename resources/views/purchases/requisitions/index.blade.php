@extends('layouts.admin')

@section('title', trans_choice('Requisitions', 2))

@permission('create-purchases-requisitions')
@section('new_button')
<span class="new-button"><a href="{{ url('purchases/requisitions/create') }}" class="btn btn-success btn-sm"><span class="fa fa-plus"></span> &nbsp;{{ trans('general.add_new') }}</a></span>
<span><a href="{{ url('common/import/purchases/requisitions') }}" class="btn btn-success btn-sm"><span class="fa fa-download"></span> &nbsp;{{ trans('import.import') }}</a></span>
@endsection
@endpermission

@section('content')
<!-- Default box -->
<div class="box box-success">
    <div class="box-header with-border">
        {!! Form::open(['url' => 'purchases/requisitions', 'role' => 'form', 'method' => 'GET']) !!}
        <div class="pull-left">
            <span class="title-filter hidden-xs">status:</span>
            {!! Form::select('status', $status, request('status'), ['class' => 'form-control input-filter input-sm']) !!}
            {!! Form::button('<span class="fa fa-filter"></span> &nbsp;' . trans('general.filter'), ['type' => 'submit', 'class' => 'btn btn-sm btn-default btn-filter']) !!}
        </div>
        <div class="pull-right">
            <span class="title-filter hidden-xs">{{ trans('general.show') }}:</span>
            {!! Form::select('limit', $limits, request('limit', setting('general.list_limit', '25')), ['class' => 'form-control input-filter input-sm', 'onchange' => 'this.form.submit()']) !!}
        </div>
        {!! Form::close() !!}
    </div>

    <!-- /.box-header -->

    <div class="box-body">
        <div class="table">
            <table class="table table-striped table-hover" id="tbl-bills">
                <thead>
                    <tr>
                        <th class="col-md-2">@sortablelink('id', trans_choice('ID', 1))</th>
                        <th class="col-md-3 hidden-xs">@sortablelink('company_id', trans_choice('Project Name', 1))</th>
                        <th class="col-md-1">@sortablelink('amount', trans('Amount'))</th>
                        <th class="col-md-2 hidden-xs">@sortablelink('billed_at', trans('Request Date'))</th>
                        <th class="col-md-2 hidden-xs">@sortablelink('due_at', trans('Priority'))</th>
                        <th class="col-md-1">@sortablelink('request_status_code', trans_choice('Status', 1))</th>
                        <th class="col-md-1 text-center">{{ trans('general.actions') }}</th>
                    </tr>
                </thead>
                <tbody>

                @foreach($requisitions as $item)
                    <tr>
                        <td>
                            <a href="/purchases/requisitions/{{ $item->id }}">
                                RN-{{ str_pad( $item->id, 6, '0', STR_PAD_LEFT) }}
                            </a>
                        </td>
                        <td class="hidden-xs">
                            @if($item->company)
                                {{ $item->company->domain }} 
                            @endif
                        </td>
                        <td>
                            @money($item->amount, 'PHP', true)
                        </td>
                        <td class="hidden-xs">
                            @if($item->created_at)
                                {{ Date::parse($item->created_at)->format('d M Y') }}
                            @else
                                 --
                            @endif
                        </td>
                        <td class="hidden-xs">
                            {{ $item->priority }}
                        </td>
                        <td>
                            <span class="badge {{ $status_codes[$item->request_status_code] }}">
                                {{ $item->request_status_code }}
                            </span>
                        </td>
                        <td>
                            
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" data-toggle-position="left" aria-expanded="false">
                                    <i class="fa fa-ellipsis-h"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    
                                    @permission('manage-purchases-requisitions')
                                        <li>
                                            <a href="{{ url('purchases/requisitions/' . $item->id . '/approve') }}">Approve</a>
                                        </li>
                                        <li class="divider"></li> 
                                        <li>
                                            <a href="{{ url('purchases/requisitions/' . $item->id . '/decline') }}">Decline</a>
                                        </li>
                                        <li class="divider"></li> 
                                    @endpermission

                                    @permission('update-purchases-requisitions')
                                        <li>
                                            <a href="{{ url('purchases/requisitions/' . $item->id . '/edit') }}">{{ trans('general.edit') }}</a>
                                        </li>
                                        <li class="divider"></li> 
                                    @endpermission
                                    @permission('delete-purchases-requisitions')
                                    <li>{!! Form::deleteLink($item, 'purchases/requisitions') !!}</li>
                                    @endpermission
                                </ul>
                            </div>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- /.box-body -->

    <div class="box-footer">
        
    </div>
    <!-- /.box-footer -->
</div>
<!-- /.box -->
@endsection

