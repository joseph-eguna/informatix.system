@extends('layouts.admin')

@section('title', trans_choice('RN-'.str_pad( $requisition->id, 6, '0', STR_PAD_LEFT), 1))

@section('content')
    <div class="box box-success">
        <div class="bill">
            <span class="badge {{ $status_codes[$requisition->request_status_code] }}">{{ $requisition->request_status_code }}</span>

            <div class="row invoice-header">
                <div class="col-xs-4 col-md-7">
                    <img src="/public/img/company.png" style="max-width: 100%;" class="invoice-logo">
                </div>
                <div class="col-xs-8 col-md-5 invoice-company">
                    <address>
                        <strong> {{ $requisition->account()->where('key','general.company_name')->first()->value }} </strong>
                        <br>
                         {{ $requisition->account()->where('key','general.company_address')->first()->value }}
                        <br><br>
                        {{ $requisition->account()->where('key','general.company_email')->first()->value }}
                    </address>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-5 col-md-8">
                    Requested by:
                    <address>
                        <strong>{{ $requisition->createdby->name }}</strong>
                    </address>

                    @if($requisition->checkedby)
                        Checked by:
                        <address>
                            <strong>{{ $requisition->checkedby->name }}</strong>
                        </address>
                    @endif
                    
                </div>
                <div class="col-xs-7 col-md-4 text-right">
                    Requisition Date:
                    <address>
                        <strong>{{ Date::parse($requisition->created_at)->format('d M Y') }}</strong>
                    </address>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 table-responsivex">
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <th>Item</th>
                                <th class="text-center">Quantity</th>
                                <th class="text-right">Price</th>
                                <th class="text-right">Total</th>
                            </tr>
                            @foreach($requisition->items as $item)
                            <tr>
                                <td>{{ $item->name }}</td>
                                <td class="text-center">{{ $item->quantity }}</td>
                                <td class="text-right">@money($item->price, 'PHP', true)</td>
                                <td class="text-right">@money($item->total, 'PHP', true)</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-5 col-xs-8 pull-right">
                    <div class="table-responsivex">
                        <table class="table">
                            <tbody>
                                    <tr>
                                        <th>Subtotal:</th>
                                        <td class="text-right">@money($requisition->amount, 'PHP', true)</td>
                                    </tr>
                                    <tr>
                                        <th>Total:</th>
                                        <td class="text-right">@money($requisition->amount, 'PHP', true)</td>
                                    </tr>
                                                                                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="box-footer row no-print">
                <div class="col-xs-12">
                    <a href="/purchases/requisitions/{{ $requisition->id }}/edit" class="btn btn-primary">
                        <i class="fa fa-pencil-square-o"></i>&nbsp; Edit
                    </a>
                    <a href="#/purchases/requisitions/{{ $requisition->id }}/print" class="btn btn-default">
                        <i class="fa fa-print"></i>&nbsp; Print
                    </a>
                    <div class="btn-group dropup">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-chevron-circle-up"></i>&nbsp; More Actions</button>
                        <ul class="dropdown-menu" role="menu">
                            @permission('manage-purchases-requisitions')
                            <li>
                                <a href="/purchases/requisitions/{{ $requisition->id }}/decline">Decline</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="/purchases/requisitions/{{ $requisition->id }}/approve">Approve</a>
                            </li>
                            <li class="divider"></li>
                            @endpermission
                            <li>
                                @permission('delete-purchases-requisitions')
                                    <li>{!! Form::deleteLink($requisition, 'purchases/requisitions') !!}</li>
                                @endpermission
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@push('js')
    <script src="{{ asset('vendor/almasaeed2010/adminlte/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('public/js/bootstrap-fancyfile.js') }}"></script>
@endpush

@push('css')
    <link rel="stylesheet" href="{{ asset('vendor/almasaeed2010/adminlte/plugins/datepicker/datepicker3.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/bootstrap-fancyfile.css') }}">
@endpush
