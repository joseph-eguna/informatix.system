<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRequisitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('requisitions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->integer('user_id');
            $table->integer('checker_id');
            $table->string('request_status_code');
            $table->enum('priority', ['normal', 'urgent']);
            $table->double('amount', 15, 4);
            $table->string('remarks');
            $table->timestamps();
            $table->softDeletes();

            $table->index('company_id', 'created_at');
        });

        Schema::create('requisitions_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('requisition_id');
            $table->string('name');
            $table->string('sku')->nullable();
            $table->integer('quantity');
            $table->double('price', 15, 4);
            $table->double('total', 15, 4);
            $table->timestamps();
            $table->softDeletes();

            $table->index('requisition_id', 'created_at');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('requisitions');
        Schema::drop('requisitions_items');
    }
}
